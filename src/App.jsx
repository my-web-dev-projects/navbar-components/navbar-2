import { Component } from "react";
import "./App.css";

class Navbar1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: false,
      navbarItems: {
        1: {
          title: "Home",
          icon: "spacing fa-solid fa-house",
          url: "#",
          styleName: "nav-links"
        },
        2: {
          title: "About",
          icon: "spacing fa-solid fa-circle-info",
          url: "#",
          styleName: "nav-links"
        },
        3: {
          title: "Services",
          icon: "spacing fa-solid fa-briefcase",
          url: "#",
          styleName: "nav-links"
        },
        4: {
          title: "Contact",
          icon: "spacing fa-solid fa-address-book",
          url: "#",
          styleName: "nav-links"
        },
        5: {
          title: "Account",
          url: "#",
          styleName: "nav-links"
        }
      }
    };
  }
  handleClick = () => {
    this.setState({ clicked: !this.state.clicked });
  };
  render() {
    const { clicked, navbarItems } = this.state;
    return (
      <>
      <nav className="NavbarItems">
        <h1>
          React <i className="fab fa-react"></i>
        </h1>
        <div className="navbar-menu" onClick={this.handleClick}>
          <i
            className={clicked ? "fas fa-times" : "fas fa-bars"}
            alt="menu"
          ></i>
        </div>
        <ul className={clicked ? "nav-menu active" : "nav-menu"}>
          {Object.entries(navbarItems).map(([navbarItemId, navbarItem]) => (
            <li key={navbarItemId}>
              <a className={navbarItem.styleName} href={navbarItem.url}>
                <i className={navbarItem.icon}></i>
                <span>{navbarItem.title}</span>
              </a>
            </li>
          ))}
        </ul>
      </nav>
      <div className="thanks__note">
        <span>
          Thank you
          <a href="https://youtu.be/q4mr71mFBr4" target="_blank">
            Tech2 etc
          </a>
          For providing well informative video on YouTube
        </span>
      </div>
    </>
    );
  }
}
export default Navbar1;
